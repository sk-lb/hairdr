from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('HairDr/', include('HairDr.urls')),
    path('admin/', admin.site.urls),
]